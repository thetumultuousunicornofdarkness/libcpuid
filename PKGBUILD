# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Contributor: X0rg

pkgname=libcpuid
pkgver=0.6.5
pkgrel=1
pkgdesc='A small C library for x86 CPU detection and feature extraction'
arch=('x86_64')
url='http://libcpuid.sourceforge.net'
license=('BSD')
depends=('glibc')
makedepends=(
  'git'
  'cmake'
  'ninja'
  'doxygen'
  'python'
  'graphviz'
)
provides=('libcpuid.so')
_commit=a578c8d993bb27ecf413ac8ff6b93845fc2fe1b3  # tags/v0.6.5
source=("$pkgname::git+https://github.com/anrieff/libcpuid#commit=$_commit")
b2sums=('307266aaa9d9ab57e489a066507b2ecab6ac93bba3fed3446d9ac8b7b3a51d1920469a9d95dfed4f15a59f005936124c7054df746a535ad8cb0cb52bc6749a4c')

pkgver() {
  cd "$pkgname"

  git describe --tags | sed 's/^v//'
}

build() {
	cmake \
    -S "$pkgname" \
    -B build \
    -G Ninja \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D LIBCPUID_TESTS=ON

	cmake --build build
}

check() {
  cd build

  ninja consistency test-old
}

package() {
	DESTDIR="$pkgdir" cmake --install build

  # license
	install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" "$pkgname/COPYING"
}
